library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TBDecodificador is
end TBDecodificador;

architecture testbench of TBDecodificador is
    component keypad
        port (row: in std_logic_vector (3 downto 0);
              col: in std_logic_vector (3 downto 0);
              BCD: out std_logic_vector (3 downto 0)
              );
    end component keypad;
    
signal row: std_logic_vector (3 downto 0) := "1111";
signal col: std_logic_vector (3 downto 0) := "1111";
signal BCD: std_logic_vector (3 downto 0) := "UUUU";

begin
    UTT: keypad
    port map(row=>row, col=>col, BCD=>BCD);
    
    TBDATA: process
    begin
    
    wait for 100 ns;
    row <= "0111";
    col <= "0111";
    wait for 100 ns;
    for r in 0 to 3 loop
        wait for 100 ns;
        for c in 0 to 3 loop
            col <= '1' & col(3 downto 1);
            wait for 100 ns;
        end loop;
        row <= '1' & row(3 downto 1);
        col <= "0111";
    end loop;
    wait for 100 ns;
    wait;
    end process TBDATA;

end testbench;
