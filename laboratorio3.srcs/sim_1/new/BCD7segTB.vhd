library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity BCD7segTB is    
end BCD7segTB;

architecture testbench of BCD7segTB is
    component BCD7seg
        port(BCDin: in integer range 0 to 15;
             Segs : out std_logic_vector (7 downto 0));
    end component BCD7seg;

signal BCDin : integer range 0 to 15;
signal Segs  : std_logic_vector(7 downto 0);

begin
    UUT: BCD7seg
        port map(BCDin => BCDin, Segs => Segs);
        
    TBDATA: process
    begin
        For input in 0 to 11 loop
            BCDIn <= Input;
            wait for 100 ns;
        end loop;
        wait;
    end process TBDATA;
end testbench;

