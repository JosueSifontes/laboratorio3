library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity BCD7seg is
port(BCDin : in integer range 0 to 15;
    Segs : out std_logic_vector(7 downto 0));
end BCD7seg;

architecture Behavioral of BCD7seg is
begin
    decoder: process (BCDin)
    begin
        case BCDin is
            when 0=>
                Segs <= "00000010";
            when 1=>
                Segs <= "10011110";
            when 2=>
                Segs <= "00100100";
            when 3=>
                Segs <= "00001100";    
            when 4=>
                Segs <= "10011000";    
            when 5=>
                Segs <= "01001000";
            when 6=>
                Segs <= "01000000";
            when 7=>
                Segs <= "00011110";
            when 8=>
                Segs <= "00000000";    
            when 9=>
                Segs <= "00001000";
            when others =>
                Segs <= "11111111";
        end case;    
    end process decoder;          
end Behavioral;

