library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Decodificador is
    Port ( 
        row: in std_logic_vector  (3 downto 0) := (others => '1');
        col: in std_logic_vector  (3 downto 0) := (others => '1');
        BCD: out std_logic_vector  (3 downto 0) := (others => 'U')
    );
end Decodificador;

architecture Behavioral of Decodificador is
begin
keypadprocess:
    process(row, col)
    begin
        if row = "1111" then
            BCD <= "UUUU";
        elsif row = "0111" then
            case col is
                when "0111" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(0,4));
                when "1011" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(1,4));
                when "1101" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(2,4));
                when "1110" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(3,4));
                when others => 
                    BCD <= (others => 'U');
            end case;
        elsif row = "1011" then
            case col is
                when "0111" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(4,4));
                when "1011" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(5,4));
                when "1101" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(6,4));
                when "1110" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(7,4));
                when others => 
                    BCD <= (others => 'U');
            end case;
        elsif row = "1101" then
            case col is
                when "0111" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(8,4));
                when "1011" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(9,4));
                when "1101" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(10,4));
                when "1110" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(11,4));
                when others => 
                    BCD <= (others => 'U');
            end case;
        elsif row = "1110" then
            case col is
                when "0111" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(12,4));
                when "1011" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(13,4));
                when "1101" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(14,4));
                when "1110" =>
                    BCD <= std_logic_vector (TO_UNSIGNED(15,4));
                when others => 
                    BCD <= (others => 'U');
            end case;
        end if;
    end process;
end Behavioral;
